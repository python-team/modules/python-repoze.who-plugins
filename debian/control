Source: python-repoze.who-plugins
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Stefano Zacchiroli <zack@debian.org>,
 Piotr Ożarowski <piotr@debian.org>
Build-Depends:
 debhelper-compat (= 9),
 cdbs (>= 0.4.90~),
 python-dev,
 dh-python,
 python-setuptools
Standards-Version: 3.8.3
Vcs-Git: https://salsa.debian.org/python-team/packages/python-repoze.who-plugins.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-repoze.who-plugins

Package: python-repoze.who-plugins
Architecture: all
Depends:
 ${misc:Depends},
 ${python:Depends},
 python-repoze.who,
 python-zopeinterface,
 python-paste (>= 1.7),
 python-pastedeploy (>= 1.3.3),
 python-openid (>= 2.0),
 python-pkg-resources,
 python-sqlalchemy (>= 0.5),
 python-ldap (>= 2.3.5)
Description: authentication framework for Python WSGI applications - plugins collection
 repoze.who is an identification and authentication framework for
 arbitrary Python WSGI applications; it acts as WSGI middleware and is
 inspired by Zope 2's Pluggable Authentication Service (PAS).
 .
 This package contains a collection of plugins for repoze.who, in
 particular:
 .
  * repoze.who-friendlyform - developer-friendly forms
  * repoze.who-plugins.sa - SQLAlchemy integration
  * repoze.who-testutil - test utilities for repoze.who applications
  * repoze.who.plugins.ldap - LDAP authentication
  * repoze.who.plugins.openid - login via OpenID
