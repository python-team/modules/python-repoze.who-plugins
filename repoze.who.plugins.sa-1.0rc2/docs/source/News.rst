*************************************
:mod:`repoze.who.plugins.sa` releases
*************************************

This document describes the releases of :mod:`repoze.who.plugins.sa`.


.. _repoze.who.plugins.sa-1.0rc2:

:mod:`repoze.who.plugins.sa` 1.0rc2 (2009-06-27)
================================================

* Added :class:`repoze.who.plugins.sa.SQLAlchemyUserChecker`, a user checker
  for :class:`repoze.who.plugins.auth_tkt.AuthTktCookiePlugin`.


.. _repoze.who.plugins.sa-1.0rc1:

:mod:`repoze.who.plugins.sa` 1.0rc1 (2009-01-26)
================================================
* Introduced the :class:`repoze.who.plugins.sa.SQLAlchemyUserMDPlugin` metadata
  provider.
* Minor docstring fixes.


.. _repoze.who.plugins.sa-1.0b3:

:mod:`repoze.who.plugins.sa` 1.0b3 (2009-01-08)
===============================================

Fixed `Bug #56 <http://bugs.repoze.org/issue56>`_ (``User.user_name`` was
not translatable).


.. _repoze.who.plugins.sa-1.0b2:

:mod:`repoze.who.plugins.sa` 1.0b2 (2008-12-18)
===============================================

Renamed :mod:`repoze.who.plugins.sqlalchemy` to :mod:`repoze.who.plugins.sa`
due to problems with the namespace.


.. _repoze.who.plugins.sqlalchemy-1.0b1:

:mod:`repoze.who.plugins.sqlalchemy` 1.0b1 (2008-12-18)
=======================================================

Initial release.
